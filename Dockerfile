FROM python:3.8

WORKDIR /app

# Install requirements
COPY requirements.txt .
RUN pip install -r requirements.txt

# Add source code
COPY src .

ENV PORT=5000

EXPOSE 5000

ENV FLASK_ENV=development

ENV FLASK_APP=app.py

# Run
CMD [ "flask", "run" ]


